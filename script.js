var canvas = new fabric.Canvas('c');

// ImageLoader

// - Image upload in Canvas 
var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);
var ctx = canvas.getContext('2d');
var imageLoaded = new Image();

function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
          console.log(img.src);
            canvas.setBackgroundImage(img.src, canvas.renderAll.bind(canvas), {scaleX: canvas.width / img.width,
                scaleY: canvas.height / img.height});
        }
        img.src = event.target.result;
        imageLoaded = img
    }
    reader.readAsDataURL(e.target.files[0]);     
}


rect = new fabric.Rect({
    left: 40,
    top: 40,
    width: 100,
    height: 100,      
    fill: 'transparent',
    stroke: 'red',
    strokeWidth: 3,
			  });  
  canvas.add(rect);


$("#save").on("click", function(e) {
  console.log(canvas.toDataURL("image/png"));
  	document.getElementById('mydiv').innerHTML = '<img src="'+canvas.toDataURL("image/png")+'"/>';
});